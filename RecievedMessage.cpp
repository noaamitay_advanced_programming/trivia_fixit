#include <Windows.h>
#include <vector>
#include "RecievedMessage.h"

using namespace std;

RecievedMessage::RecievedMessage(SOCKET clientSocket, int msgCode, vector<string> values)
{
	_sock = clientSocket;
	_messageCode = msgCode;
	_values = values;
}

RecievedMessage::~RecievedMessage()
{

}

SOCKET RecievedMessage::getSock()
{
	return _sock;
}

User* RecievedMessage::getUser()
{
	return _user;
}
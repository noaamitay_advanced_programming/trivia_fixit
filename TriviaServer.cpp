#include "TriviaServer.h"

std::condition_variable cv;

TriviaServer::TriviaServer()
{
	//_db = new DataBase();

	WSADATA wsa_data = {};
	if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
		throw std::exception("WSAStartup Failed");

	_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (_socket == INVALID_SOCKET)
	{
		throw std::exception("invalid socket");
	}
	std::cout << "im here!" << std::endl;
	server();
}

TriviaServer::~TriviaServer()
{
	closesocket(_socket);
	//	_roomList.clear();
	//	_connectedUsers.clear();
}

void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };
	sa.sin_port = htons(PORT); // The port that the server will listen to
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY; //Any ip

									 //Connect between the socket and the port, ip, etc..
	if (::bind(_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}

	//Start to listen to requestes of clients
	if (::listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}
	std::cout << "bind and listen - check!" << std::endl;
}

void TriviaServer::server()
{
	bindAndListen();
	std::thread t([=] { handleRecievedMessages(); });
	t.detach();

	while (true)
	{
		accept();
	}
}

void TriviaServer::accept()
{
	std::cout << "im in accept!" << std::endl;
	SOCKET clientSocket = ::accept(_socket, NULL, NULL);

	if (clientSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__);
	}

	std::thread t([=] { clientHandler(clientSocket); });
	t.detach();
}

void TriviaServer::clientHandler(SOCKET clientSocket)
{
	int msgTypeCode = -1;
	std::string msg = "";
	std::cout << "connected!" << std::endl;

	try
	{
		while (msgTypeCode != 0 || msgTypeCode != 299)
		{
			msg = Helper::getStringPartFromSocket(clientSocket, 1024);
			msgTypeCode = atoi(msg.substr(0, 3).c_str());
			RecievedMessage* rm = buildRecievedMessage(clientSocket, msgTypeCode, msg);
			addRecievedMessage(rm);
		}

		addRecievedMessage(buildRecievedMessage(clientSocket, msgTypeCode, "299"));
	}
	catch (...)
	{
		addRecievedMessage(buildRecievedMessage(clientSocket, 299, "299"));
	}
}

RecievedMessage* TriviaServer::buildRecievedMessage(SOCKET socket, int msgCode, std::string msg)
{
	std::vector<std::string> values;
	std::string msgValues = msg.substr(3, msg.length() - 3);

	//switch case can't create a new string
	std::string username = "", password = "", email = "", roomId = "", roomName = "", playersNumber = "", questionsNumber = "", questionTimeInSec = "", answerNumber = "", timeInSeconds = "";
	int usernameLen = 0, passwordLen = 0, roomNameLen = 0, emailLen = 0;

	switch (msgCode)
	{
	case 200:
		usernameLen = atoi(msgValues.substr(0, 2).c_str());
		passwordLen = atoi(msgValues.substr(usernameLen + 2, 2).c_str());
		username = msgValues.substr(2, usernameLen);
		password = msgValues.substr(usernameLen + 4, passwordLen);
		values.push_back(username);
		values.push_back(password);
		break;
	case 203:
		usernameLen = atoi(msgValues.substr(0, 2).c_str());
		passwordLen = atoi(msgValues.substr(usernameLen + 2, 2).c_str());
		emailLen = atoi(msgValues.substr(usernameLen + passwordLen + 4, 2).c_str());
		username = msgValues.substr(2, usernameLen);
		password = msgValues.substr(usernameLen + 4, passwordLen);
		email = msgValues.substr(usernameLen + passwordLen + 6, emailLen);
		values.push_back(username);
		values.push_back(password);
		values.push_back(email);
		break;
	case 207:
		roomId = msgValues.substr(3, 4);
		values.push_back(roomId);
		break;
	case 209:
		roomId = msgValues.substr(3, 4);
		values.push_back(roomId);
		break;
	case 213:
		roomNameLen = atoi(msgValues.substr(0, 2).c_str());
		roomName = msgValues.substr(2, roomNameLen);
		playersNumber = msgValues.substr(roomNameLen + 2, 1);
		questionsNumber = msgValues.substr(roomNameLen + 3, 2);
		questionTimeInSec = msgValues.substr(roomNameLen + 5, 2);
		values.push_back(roomName);
		values.push_back(playersNumber);
		values.push_back(questionsNumber);
		values.push_back(questionTimeInSec);
		break;
	case 219:
		answerNumber = msgValues.substr(0, 1);
		timeInSeconds = msgValues.substr(1, 2);
		values.push_back(answerNumber);
		values.push_back(timeInSeconds);
		break;
	}

	RecievedMessage* rm = new RecievedMessage(socket, msgCode, values);
	return rm;
}

void TriviaServer::addRecievedMessage(RecievedMessage* msg)
{
	std::unique_lock<std::mutex> lock(_mtxRecievedMessages);
	_queRcvMessages.push(msg);
	lock.unlock();
	cv.notify_one();
}

void TriviaServer::handleSignout(RecievedMessage* msg)
{
	if (msg->getUser() != nullptr)
	{
		_connectedUsers.erase(msg->getSock());
	}

	bool closeRoom = handleCloseRoom(msg);
	bool leaveRoom = handleLeaveRoom(msg);
	handleLeaveGame(msg);
}

bool TriviaServer::handleCloseRoom(RecievedMessage* msg)
{
	return true;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage* msg)
{
	return true;
}

void TriviaServer::handleLeaveGame(RecievedMessage* msg)
{

}

bool TriviaServer::handleCreateRoom(RecievedMessage* msg)
{
	bool succeeded = false;
	User* user = msg->getUser();

	if (user != nullptr)
	{
		//_roomIdSequence++;
	}

	return succeeded;
}

void TriviaServer::handleRecievedMessages()
{
	while (true)
	{
		std::unique_lock<std::mutex> lock(_mtxRecievedMessages);
		cv.wait(lock);

		if (!_queRcvMessages.empty())
		{

			RecievedMessage* rm = _queRcvMessages.front();
			_queRcvMessages.pop();

			lock.unlock();

			/*try
			{
			rm->setUser(getUserBySocket(rm->getSock()));

			switch (rm->getMessageCode())
			{
			case 200:
			handleSignin(rm);
			break;
			case 201:
			handleSignout(rm);
			break;
			case 203:
			handleSignup(rm);
			break;
			case 205:
			handleGetRooms(rm);
			break;
			case 207:
			handleGetUsersInRoom(rm);
			break;
			case 209:
			handleJoinRoom(rm);
			break;
			case 211:
			handleLeaveRoom(rm);
			break;
			case 213:
			handleCreateRoom(rm);
			break;
			case 215:
			handleCloseRoom(rm);
			break;
			case 217:
			handleStartGame(rm);
			break;
			case 219:
			handlePlayerAnswer(rm);
			break;
			case 222:
			handleLeaveGame(rm);
			break;
			case 223:
			handleGetBestScores(rm);
			break;
			case 225:
			handleGetPersonalStatus(rm);
			break;
			case 299:
			safeDeleteUser(rm);
			break;
			default:
			safeDeleteUser(rm);
			break;
			}
			}
			catch (...)
			{
			safeDeleteUser(rm);
			}*/
		}
	}
}

/*
User* TriviaServer::handleSignin(RecievedMessage* msg)
{

}*/